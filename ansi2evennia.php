<?php
function convertAnsiToColor($ansi)
{
  $bg = 0;
  $fg = 7;
  if ('0' != $ansi && '' != $ansi) {
    $options = explode(';', $ansi);

    foreach ($options as $option) {
      if ($option >= 30 && $option < 38) {
        $fg = $option - 30;
      } elseif ($option >= 40 && $option < 48) {
        $bg = $option - 40;
      } elseif (39 == $option) {
        $fg = 7;
      } elseif (49 == $option) {
        $bg = 0;
      }
    }

    // options: bold => 1, underscore => 4, blink => 5, reverse => 7, conceal => 8
    if (in_array(1, $options)) {
      $fg += 10;
      $bg += 10;
    }

    if (in_array(7, $options)) {
      $tmp = $fg;
      $fg = $bg;
      $bg = $tmp;
    }
  }
  $colorNames = array(
    'x', 'R', 'G', 'Y', 'B', 'M', 'C', 'W',
    '', '',
    'u', 'r', 'g', 'y', 'b', 'm', 'c', 'w',
  );
  if ($bg === 'black') {
    return sprintf('|%s', $colorNames[$fg]);
  }

  return sprintf('|%s|[%s', $colorNames[$fg], $colorNames[$bg]);
}
function tokenize($text)
{
  $tokens = array();
  preg_match_all("/(?:\e\[(.*?)m|(\x08))/", $text, $matches, PREG_OFFSET_CAPTURE);

  $offset = 0;
  foreach ($matches[0] as $i => $match) {
    if ($match[1] - $offset > 0) {
      $tokens[] = array('text', substr($text, $offset, $match[1] - $offset));
    }
    $tokens[] = array("\x08" == $match[0] ? 'backspace' : 'color', $matches[1][$i][0]);
    $offset = $match[1] + strlen($match[0]);
  }
  if ($offset < strlen($text)) {
    $tokens[] = array('text', substr($text, $offset));
  }

  return $tokens;
}
function convert($text)
{
  // remove cursor movement sequences
  $text = preg_replace('#\e\[(K|s|u|2J|2K|\d+(A|B|C|D|E|F|G|J|K|S|T)|\d+;\d+(H|f))#', '', $text);
  // remove character set sequences
  $text = preg_replace('#\e(\(|\))(A|B|[0-2])#', '', $text);

  // carriage return
  $text = preg_replace('#^.*\r(?!\n)#m', '', $text);

  $tokens = tokenize($text);

  // a backspace remove the previous character but only from a text token
  foreach ($tokens as $i => $token) {
    if ('backspace' == $token[0]) {
      $j = $i;
      while (--$j >= 0) {
        if ('text' == $tokens[$j][0] && strlen($tokens[$j][1]) > 0) {
          $tokens[$j][1] = substr($tokens[$j][1], 0, -1);

          break;
        }
      }
    }
  }

  $html = '';
  foreach ($tokens as $token) {
    if ('text' == $token[0]) {
      $html .= $token[1];
    } elseif ('color' == $token[0]) {
      $html .= convertAnsiToColor($token[1]);
    }
  }

  $html = sprintf('%s', $html);

  return $html;
}

$out = '';
$handle = fopen("vantamoo_unicode.txt", "r");
if ($handle) {
  while (($line = fgets($handle)) !== false) {
    $out .= convert($line).PHP_EOL;
  }

  fclose($handle);
}

file_put_contents('out.txt', $out);
